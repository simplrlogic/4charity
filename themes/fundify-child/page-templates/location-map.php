<?php
/**
 * Template Name: Location Map
 *
 * Show Charity house's location map
 */

global $wp_query, $wp_rewrite;

$charity_house = '';
$page_name = '';

if(isset($wp_query->query_vars['charity_house'])) {
  $charity_house = urldecode($wp_query->query_vars['charity_house']);
}

$author = get_user_by( 'login', $charity_house );

get_header(); ?>

	<div class="title title-two pattern-<?php echo rand(1,4); ?>">
		<div class="container">
			<h1><?php echo $author->display_name; ?></h1>
			<h3><?php echo 'Location Map'; ?></h3>
		</div>
		<!-- / container -->
	</div>
  <div id="content">
               
		<div class="container">
			<?php if ( '' != $author->user_description ) : ?>
			<div class="single-author-bio">
				<?php echo get_avatar( $author->user_email, 280 ); ?>
				<ul class="author-bio-links">
                <li>
                <?php if ( 'hoj' == $author->user_login ) : ?>
		            <?php 
				     $address = ' Address:
                        No. 78A, Jalan TK 1/1, 
                        Taman Kinrara, Seksyen 1,
                        47180 Puchong, Selangor.
 		
                      Further enquiries, please contact:  Lesley Mah : +6019-3392847'; 
				    echo wpautop( $address ); 
				    ?>
                 <?php elseif ( 'rumah_charis' == $charity_house ) : ?>
                     <?php 
				    $address = 'Address:
                               77, Taman Lucky, 
 		                       Batu 6 off Jalan Puchong,
 		                       58200 KL'; 
				    echo wpautop( $address ); ?>
                 <?php elseif ( 'shelter' == $charity_house ) : ?>
                     <?php 
				    $address = 'Address:
                                No. 4, Jalan Tinggi 6/12, 
 		                        46200 Petaling Jaya,
                                Selangor'; 
				    echo wpautop( $address ); ?>
				    
				    <?php elseif ( 'sunbeams' == $charity_house ) : ?>
                    <?php 
				    $address = 'Address:
                                54, Jalan Bunga Melur 16A,
                                Taman Mawar, 
 		                        56100 Kuala Lumpur'; 
				    echo wpautop( $address ); ?>
				    
                 <?php endif; ?>	
	</li> <li><?php echo do_shortcode('[printfriendly]'); ?></li>

                          
				</ul >
				<ul class="author-bio-links"></ul>
			</div>
			<?php endif; ?>

			<div id="projects">
			
			 <!-- TODO: this should be auto-populated ! -->
                 <?php if ( 'hoj' == $author->user_login ) : ?>
                     <?php echo do_shortcode('[easy2map id=2]'); ?>
                 <?php elseif ( 'rumah_charis' == $charity_house ) : ?>
                     <?php echo do_shortcode('[easy2map id="5"]'); ?>
                 <?php elseif ( 'shelter' == $charity_house ) : ?>
                     <?php echo do_shortcode('[easy2map id="6"]'); ?>
                 <?php elseif ( 'sunbeams' == $charity_house ) : ?>
                     <?php echo do_shortcode('[easy2map id="8"]'); ?>
                 <?php endif; ?>
                      
				<?php do_action( 'fundify_loop_after' ); ?>
			</div>
		</div>
		<!-- / container -->
	</div>
	<!-- / content -->

<?php get_footer(); ?>