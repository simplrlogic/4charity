<?php
/**
 * Template Name: Success Stories
 *
 * @package Fundify
 * @since Fundify 1.3 Customized template to show completed campaigns.
 */

global $wp_query;
get_header();
?>
	<div id="content">
		<div class="container">
			<div id="projects">
				<section>
					<?php 
						if ( fundify_is_crowdfunding()  ) :
							$wp_query = new ATCF_Campaign_Query( array(
								'paged' => ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 )
							) );
						else :
							$wp_query = new WP_Query( array(
								'posts_per_page' => get_option( 'posts_per_page' ),
								'paged'          => ( get_query_var('page') ? get_query_var('page') : 1 )
							) );
						endif;

						if ( $wp_query->have_posts() ) :
					?>

						<?php while ( $wp_query->have_posts() ) : 
						              $wp_query->the_post(); 
						              $campaign = atcf_get_campaign( $post ); ?>
						    <?php if ( $campaign->is_funded() ) {
							        get_template_part( 'content', fundify_is_crowdfunding() ? 'campaign-success' : 'post' ); 
						          } else {
                                    continue;
                                  }
						     ?>
						<?php endwhile; ?>

					<?php else : ?>

						<?php get_template_part( 'no-results', 'index' ); ?>

					<?php endif; ?>
				</section>

				<?php do_action( 'fundify_loop_after' ); ?>
			</div>
		</div>
		<!-- / container -->
</div>
<?php get_footer(); ?>