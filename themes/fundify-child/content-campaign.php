<?php
/**
 * @package Fundify
 * @since Fundify 1.0
 */

global $post;

$campaign = atcf_get_campaign( $post );
$currency_prefix = get_campaign_currency($campaign);

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'item' ); ?>>
	<?php if ( $campaign->hours_remaining() <= 12 && $campaign->is_active() && !$campaign->is_funded() ) : ?>
	<div class="campaign-ribbon">
		<a href="<?php the_permalink(); ?>"><?php _e( 'Ending Soon', 'fundify' ); ?></a>
	</div>
	<?php elseif ( get_post_meta( $post->ID, '_campaign_featured', true ) && !$campaign->is_funded() ) : ?>
	<div class="campaign-ribbon featured">
		<a href="<?php the_permalink(); ?>"><?php _e( 'Urgent Needs', 'fundify' ); ?></a>
	</div>
	<?php elseif ( $campaign->is_funded() ) : ?>
	<div class="campaign-ribbon success">
		<a href="<?php the_permalink(); ?>"><?php _e( 'Successful', 'fundify' ); ?></a>
	</div>
	<?php elseif ( ! $campaign->is_active() && ! $campaign->is_funded() ) : ?>
	<div class="campaign-ribbon unsuccess">
		<a href="<?php the_permalink(); ?>"><?php _e( 'Unsuccessful', 'fundify' ); ?></a>
	</div>
	<?php endif; ?>

	<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'fundify' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
		<?php the_post_thumbnail( 'campaign' ); ?>
	</a>
	
	<h3 class="entry-title">
		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'fundify' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</h3>
	  <?php the_excerpt(); ?>
       
        <!-- Add location categories -->
       <div style="margin: 10px 0 10px 15px;">
       <?php
           $terms = get_the_terms( $post->ID , fundify_is_crowdfunding() ? 'download_category' : 'category', array( 'hide_empty' => 0 ) );
           $separator = ", ";
           $location_str = "";
       ?>    
       <table>
           <!-- User avatar -->
           <tr><td style="max-width:25px; text-align:center;"><?php echo get_avatar( $campaign->contact_email(), 25 ); ?></td>                 
               <td style="text-align:left; vertical-align:bottom; padding-left:5px"><?php printf( __( '<strong>%s</strong>' ), $campaign->author() ); ?></td>
           </tr>
           <tr> <td style="max-width:25px; text-align:center;"> <img src="/wp-content/plugins/easy2map/images/map_pins/pins/444.png"> </td>
       <?php foreach ( $terms as $term ) {
             // Location category, max 2 level

             if ( $term->parent == 0 ) {
                 if ( strlen($location_str) > 0 ) {
                     $location_str = $location_str . $separator . $term->name;
                 } else {
                     $location_str = $term->name;
                 }
             } else {
                 if ( strlen($location_str) > 0 ) {
                     $location_str = $term->name . $separator . $location_str;
                 } else {
                     $location_str = $term->name;
                 }                  
             }  
         } ?>
             <td style="text-align:left; vertical-align:bottom; padding-left:5px; padding-bottom:3px"> <?php printf( __( '<strong>%s</strong>' ), $location_str ); ?></td>
          </tr>
          </table>
        </div>

	<div class="digits">
		<div class="bar"><span style="width: <?php echo $campaign->percent_completed(); ?>"></span></div>
		<ul>
		<li><?php printf( __( '<strong>%s</strong> Funded', 'fundify' ), $campaign->percent_completed() ); ?></li>
		
		<li><?php printf( _x( '<strong>%s %s</strong> Funded', 'Amount funded in single campaign stats', 'fundify' ), $currency_prefix, $campaign->current_amount() ); ?></li>
			<?php if ( ! $campaign->is_endless() ) : ?>
			<li>
				<?php if ( $campaign->days_remaining() > 0 ) : ?>
					<?php printf( __( '<strong>%s</strong> Days to Go', 'fundify' ), $campaign->days_remaining() ); ?>
				<?php else : ?>
					<?php printf( __( '<strong>%s</strong> Hours to Go', 'fundify' ), $campaign->hours_remaining() ); ?>
				<?php endif; ?>
			</li>
			<?php endif; ?>
		</ul>
	</div>
</article>