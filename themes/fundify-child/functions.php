<?php
/* BEGIN : Logout link in navigation menu */
function simplr_add_logout_item_to_menu( $items, $args ){
		
 // Default to top-right menu
 if( is_admin() ||  $args->theme_location != 'primary-right' ) {
 	return $items;
 }
 
 $redirect = home_url();
 	
 if( is_user_logged_in( ) ) {
 	// Make sure it is replacing
 	$link = '<a href="' . wp_logout_url( $redirect ) . '" title="' .  __( 'Logout' ) .'">' . __( 'Logout' ) . '</a>';
 	$custom_menu =  $items.= '<li id="log-out-link" class="menu-item menu-type-link">'. $link . '</li>';
 } else {
 	$custom_menu = $items;
 }
 
 return $custom_menu;
 
}
add_filter( 'wp_nav_menu_items', 'simplr_add_logout_item_to_menu', 50, 2 );
/* END : Logout link in navigation menu */

/* BEGIN Custom User Contact Info */
if ( ! function_exists('extra_contact_info') ) {
  function extra_contact_info( $user_contact ) {
  	unset( $contactmethods[ 'twitter' ] );
//   	unset( $contactmethods[ 'facebook' ] );
  	unset( $contactmethods[ 'aim' ] );
  	unset( $contactmethods[ 'yim' ] );
  	unset( $contactmethods[ 'jabber' ] );
	
    $user_contact ['phone_number'] = __('Phone number');
    $user_contact ['location_map'] = __('Location Map');
    $user_contact ['facebook'] = __('Facebook');
    return $user_contact;
  }
  add_filter( 'user_contactmethods', 'extra_contact_info', 10, 1 );
}
/* END Custom User Contact Info */

/**
 * simple-user-listing customization, to search user by FB user profile picture
 *
 */
function kia_remove_search(){
	global $simple_user_listing;
	remove_action( 'simple_user_listing_before_loop', array( $simple_user_listing, 'add_search' ) );
}
add_action( 'wp_head', 'kia_remove_search' );

// Switch the WP_User_Query args to a meta search
function kia_meta_search( $args ){

	// this $_GET is the name field of the custom input in search-author.php
	$search = ( isset($_GET['oa_social_login_user_token']) ) ? sanitize_text_field($_GET['oa_social_login_user_tokenl']) : false ;

	if ( $search ){
		$args['meta_key'] = 'oa_social_login_user_token';
		$args['meta_value'] = $search;
		$args['meta_compare'] = '=';
	} else { // default get all where user_token is not blank
           $args['meta_key'] = 'oa_social_login_user_token';
           $args['meta_value'] = '';
	    $args['meta_compare'] = '!=';
       }

	return $args;
}
add_filter('sul_user_query_args', 'kia_meta_search');

// Register query var and whitelist with Simple User Listing
function kia_search_vars( $vars ){
	$vars[] = 'oa_social_login_user_token';
	return $vars;
}
add_filter('sul_user_allowed_search_vars', 'kia_search_vars');
?>