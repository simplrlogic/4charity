<?php
/**
 * Plugin Name: Site Plugin for 4charity.my
 * Description: Site specific code changes for 4charity.my
 */

/** For location map template **/
function add_query_vars($aVars) {
  $aVars[] = "charity_house"; // charity house sub-page
  return $aVars;
}
 
// hook add_query_vars function into query_vars
add_filter('query_vars', 'add_query_vars');

function add_rewrite_rules($aRules) {
  $aNewRules = array( 'location-map/([^/]+)/?$' => 'index.php?pagename=location-map&charity_house=$matches[1]' );

  $aRules = $aNewRules + $aRules;
  return $aRules;
}
 
// hook add_rewrite_rules function into rewrite_rules_array
add_filter('rewrite_rules_array', 'add_rewrite_rules');

/** MUST do : Setting -> Save permenant links after add this rewrite_rules changes **/

/**
 * List out contributed campaign contributions
 *
 */
function list_contributions( $user ) {
	$user_id = get_current_user_id();
       if ($user_id == 0) {
           return wp_login_form( apply_filters( 'atcf_shortcode_profile_login_args', array() ) );
       } 
	
	global $edd_options;

	$contributions = edd_get_payments( array( 'user' => $user_id,
    			                            'status' => atcf_has_preapproval_gateway() ? array( 'preapproval', 'publish' ) : 'publish'
	                                  ) );

	if ( empty( $contributions ) )
		return;
	
	echo '<div class="atcf-profile">';
	?>
	
	<h3 class="atcf-profile-section your-campaigns"><?php _e( 'Your Contributions', 'atcf' ); ?></h3>

	<ul class="atcf-profile-contributinos">
		<?php foreach ( $contributions as $contribution ) : ?>
		<?php
			$payment_data = edd_get_payment_meta( $contribution->ID );
			$cart         = edd_get_payment_meta_cart_details( $contribution->ID );
			$key          = edd_get_payment_key( $contribution->ID );
		?>
		<?php if ( $cart ) : ?>
		<li>
			<?php foreach ( $cart as $download ) : ?>
			<?php printf( _x( '<a href="%s">%s</a> pledge to <a href="%s">%s</a>', 'price for download (payment history)', 'atcf' ), add_query_arg( 'payment_key', $key, get_permalink( $edd_options[ 'success_page' ] ) ), edd_currency_filter( edd_format_amount( $download[ 'price' ] ) ), get_permalink( $download[ 'id' ] ), $download[ 'name' ] ); ?>
			<?php endforeach; ?>
		</li>
		<?php endif; ?>
		<?php endforeach; ?>		
	</ul>
	<?php echo '</div>'; ?>
<?php 
}
add_shortcode( 'contribution_histories', 'list_contributions' );

/**
 * Get currency for the campaign, for display purpose in the main page, project details page, and email to donor
 * @param $campaign
 * @return string
 */
function get_campaign_currency( $campaign ) {
	$currency_prefix = '';

	if ( $campaign->ID == 1251 ) {
        $currency_prefix = 'RM ';
	}

	return $currency_prefix;
}

/**
 * Get the contents of the cart's currency
 *
 * @return currency either empty string or RM
 */
function edd_get_cart_currency() {
    $currency_prefix = '';
	$cart_items = edd_get_cart_contents();
	if ( $cart_items ) {
	    foreach ( $cart_items as $key => $item ) {
            if ( $item['id'] == 1251 ) {
                $currency_prefix = 'RM ';
            }
        }
    }
    
    return $currency_prefix;
}

/**
 * Get currency for the campaign, for display purpose in the receipt page
 * @param unknown $campaign
 * @return string
 */
function get_shortcode_receipt_currency( $cart ) {
	$currency_prefix = '';

	foreach ( $cart as $key => $item ) {
	    if ( $item['id'] == 1251 ) {
	       $currency_prefix = 'RM ';
	    }
	}

	return $currency_prefix;
}
?>