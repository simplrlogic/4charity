<?php
//Copy of registration function in crowd-funding shortcode-register.php which is required in ajax process

/**
 * Handles registering a new user.
 *
 * @return int|WP_Error Either user's ID or error on failure.
 */
function register_new_user() {

	$errors = new WP_Error();
       
    $nicename = isset( $_POST[ 'displayname' ] ) ? esc_attr( $_POST[ 'displayname' ] ) : null;
	$email = isset( $_POST[ 'user_email' ] ) ? esc_attr( $_POST[ 'user_email' ] ) : null;
	$username = isset( $_POST[ 'user_login' ] ) ? esc_attr( $_POST[ 'user_login' ] ) : null;
	$password = isset( $_POST[ 'user_pass' ] ) ? esc_attr( $_POST[ 'user_pass' ] ) : null;
    $phone_number = isset( $_POST[ 'phone_number' ] ) ? esc_attr( $_POST[ 'phone_number' ] ) : null;

	$sanitized_user_login = sanitize_user( $username );
	$user_email = apply_filters( 'user_registration_email', $email );

	// Check the username
	if ( $sanitized_user_login == '' ) {
		$errors->add( 'empty_username', __( '<strong>ERROR</strong>: Please enter a username.' ) );
	} elseif ( ! validate_username( $username ) ) {
		$errors->add( 'invalid_username', __( '<strong>ERROR</strong>: This username is invalid because it uses illegal characters. Please enter a valid username.' ) );
		$sanitized_user_login = '';
	} elseif ( username_exists( $sanitized_user_login ) ) {
		$errors->add( 'username_exists', __( '<strong>ERROR</strong>: This username is already registered. Please choose another one.' ) );
	}

	// Check the e-mail address
	if ( $email == '' ) {
		$errors->add( 'empty_email', __( '<strong>ERROR</strong>: Please type your e-mail address.' ) );
	} elseif ( ! is_email( $email ) ) {
		$errors->add( 'invalid_email', __( '<strong>ERROR</strong>: The email address isn&#8217;t correct.' ) );
		$user_email = '';
	} elseif ( email_exists( $email ) ) {
		$errors->add( 'email_exists', __( '<strong>ERROR</strong>: This email is already registered, please choose another one.' ) );
	}

	/** Check Password */
	if ( empty( $password ) )
		$errors->add( 'invalid-password', __( 'Please choose a secure password.', 'atcf' ) );
	
// 	do_action( 'register_post', $sanitized_user_login, $user_email, $errors );
//	$errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $user_email );
	$errors = apply_filters( 'atcf_register_validate', $errors, $_POST );

	if ( $errors->get_error_code() )
		return $errors;
	
	if ( '' == $nicename )
		$nicename = $username;
	
	$reg_details = array(
			'user_login'           => $username,
			'user_pass'            => $password,
			'user_email'           => $email,
			'display_name'         => $nicename,
	) ;
	
	$defaults = array(
			'user_pass'            => wp_generate_password( 12, false ),
			'show_admin_bar_front' => 'false',
			'role'                 => 'campaign_contributor'
	);
	
	$args = wp_parse_args( $reg_details, $defaults );
	
	$user_id = wp_insert_user($args);
	
	$secure_cookie = is_ssl() ? true : false;
	wp_set_auth_cookie( $user_id, true, $secure_cookie );
	wp_new_user_notification( $user_id, $args[ 'user_pass' ] );
	
	if ( ! empty ( $user_id->errors ) ) {
		return $user_id->errors;
	}
	
	// update extra user-meta
	update_user_meta( $user_id, 'phone_number', $phone_number);
	
	do_action( 'atcf_register_process_after', $user_id, $_POST );
	
	return $user_id;
}